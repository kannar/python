# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require setup-py [ import=distutils blacklist=3 ]

SUMMARY="A Pure-Python library built as a PDF toolkit"
DESCRIPTION="
${PN} allows for
    * extracting document information (title, author, ...),
    * splitting documents page by page,
    * merging documents page by page,
    * cropping pages,
    * merging multiple pages into a single page,
    * encrypting and decrypting PDF files.

By being Pure-Python, it should run on any Python platform without any dependencies
on external libraries. It can also work entirely on StringIO objects rather than
file streams, allowing for PDF manipulation in memory. It is therefore a useful
tool for websites that manage or manipulate PDFs.
"
HOMEPAGE="http://pybrary.net/${PN}/"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.gz"

#BUGS_TO="philantrop@exherbo.org"
REMOTE_IDS="pypi:${PN}"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/pythondoc-${PN}.pdf.html [[ lang = en ]]"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""
DEPENDENCIES=""

src_test() {
    # upstream doesn't provide a test suite
    :
}

